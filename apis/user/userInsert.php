<?php
/*-----------------------------------------+
|                                          |
|        ユーザー新規登録ＡＰＩ            |
|     -author namoyan303@gmail.com         |
|     -create 2013/08/22                   |
|     -version 0.5                         |
+------------------------------------------*/

include_once("../../classes/user/userCreate.inc");
include_once("../../classes/json/jsonRecieve.inc");

$modUser = new userCreate();
$modJson = new jsonReceive();

//必須入力済みなら
if(isset($_POST['nickname']) && isset($_POST['mailaddress']) && isset($_POST['passwd'])){
	$user_arr=array();
	foreach($_POST as $key=>$value){
		$user_arr['nickname'] = $value['nickname'];
		$user_arr['mailaddress'] = $value['mailaddress'];
		$user_arr['passwd'] = $value['passwd'];
		if(isset($value['newspaper_flg'])){
			$user_arr['newspaper_flg'] = $value['newspaper_flg'];
		}else{
			$user_arr['newspaper_flg'] = 0;
		}
	}
}else{
	//Jsonクラスでエラー掃出し?
	
	
	
}


$rows = $modUser->insert($user_arr);
$header = array();
$body = array();
if($rows['flag']){
	//Json構築
	$modJson->userReceive(null,"ユーザーの作成に成功しました。",$rows);
}else{
	$modJson->userReceive(NOT_AVAILLABLE_CODE,"ユーザーの作成に成功しました。",$rows);
}
$res = json_encode(array($header,$body));
echo $res;

