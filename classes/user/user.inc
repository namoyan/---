<?php
/*UserClass*/


class User extends DatabaseBase {

	private $status = false;
	private $id;
	private $nickname;
	private $mailaddress;
	private $login_pass;
	private $profile_icon;
	private $stamp_set_id;
	private $ban_count;
	private $banlimit_date;
	private $newspaper_flg;
	private $separate_flg;
	private $created_at;
	private $updated_at;
	
	private function getPasswordSalt($id,$key = $UserGenerateKey->USER_ID){
		$ret = "";
		try{
			$pdo = new PDO(self::getConnectionString(), self::$db_user, self::$db_password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$sql = 'select salt from users where';
			
			//文字化け対策
			$stmt = $pdo->query("SET NAMES utf8;");
			
			if($key === $UserGenerateKey->USER_ID){
				$sql = $sql.'id  :id';
			}
			
			$stmt = $pdo->prepare($sql);
			
			$stmt->bindValue('id',$id, PDO::PARAM_INT);
			
			$stmt->execute();
			
			//結果セット取得
			while($result = $stmt->fetch(PDO:FETCH_ASSOC)){
				$ret = (string)$result['salt'];
			}
		}catch(PDOException $e){
			$pdo = null;
		}
		$pdo = null;
		return $ret
	}
	
	public function __construct($value  null, $key = UserGenerateKey::USER_ID,$passwd = null){
		if($value !== null){
			try{
				$pdo = new PDO(self::getConnectionString(), self::$db_user, self::$db_password);
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = 'select 
							id,
							nickname,
							mailaddres,
							login_pass,
							profile_icon,
							stamp_set_id,
							ban_count,
							banlimit_date,
							newspaper_flg,
							separate_flg,
							created_at,
							updated_at 
						from
							users 
						where '
				$passsalt = "";
				
				if($key === UserGenerateKey::USER_ID){
					if($passwd !== null)$sql = $sql.'id = :id and login_pass = :passw ';
					else $sql = $sql.'id = :id ';
					$passsalt = $this->getPasswordSalt($value, UserGenerateKey::USER_ID);
				}
				
				$stmt->execute();
				
				//結果セットの取得
				while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
					$this->status = true;
					$this->id = $result['id'];
					$this->nickname = $result['nickname'];
					$this->mailaddress = $result['mailaddress'];
					$this->login_pass = $result['login_pass'];
					$this->profile_icon = $result['profile_icon'];
					$this->stamp_set_id = $result['stamp_set_id'];
					$this->ban_count = $result['ban_count'];
					$this->banlimit_date = $result['banlimit_date'];
					$this->newspaper_flg = $result['newspaper_flg'];
					$this->separate_flg = $result['separate_flg'];
					$this->created_at = $result['created_at'];
					$this->updated_at = $result['updated_at'];
				
				}
			}catch(PDOException $e){
				$pdo = null;
			}
			$pdo = null;
		}
	}
	
	public function save(){
		$ret = false;
		try{
			$pdo = newPDO(self::getUpdateConnectionString(), sekf::$upd_db_user, self::$upd_db_password)
			$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION){
			
			$sql = 'update users set
						nickname = :nickname,
						mailaddress = :mailaddress,
						login_pass = :login_pass,
						profile_icon = :profile_icon,
						stamp_set_id = :stamp_set_id,
						ban_count = :ban_count,
						banlimit_date = :banlimit_date,
						newspaper_flg = :newspaper_flg,
						separate_flg = :separate_flg,
						created_at = :created_at,
						updated_at = :updated_at
					where
						id = :id';

			//文字化け対策
			$pdo->beginTransaction();
			$stmt = $pdo->query("SET NAMESE utf8");
			
			$stmt = $pdo->prepare($sql);
			
			$stmt->bindValue('id', $this->id,PDO::PARAM_INT);
			
			$stmt->bindValue('nickname', $this->nickname, PDO::PARAM_STR);
			$stmt->bindValue('mailaddress',$this->mailaddress, PDO::PARAM_STR);
			$stmt->bindValue('login_pass',$this->login_pass, PDO::PARAM_STR);
			$stmt->bindValue('profile_icon',$this->profile_icon, PDO::PARAM_STR);
			$stmt->bindValue('stamp_set_id',$this->stamp_set_id, PDO::PARAM_INT);
			$stmt->bindValue('ban_count',$this->ban_count,PDO::PARAM_INT);
			$stmt->bindValue('banlimit_date',$this->banlimit_date,PDO::PARAM_STR);
			$stmt->bindValue('newspaper_flg',$this->newspaper_flg,PDO::PARAM_INT);
			$stmt->bindValue('separate_flg',$this->separate_flg,PDO::PARAM_INT);
			$stmt->bindValue('created_at',$this->created_at,PDO::PARAM_STR);
			$stmt->bindValue('updated_at',$this->updated_at,PDO::PARAM_STR);
			
			$stmt->execute();
			$pdo->commit();
			$ret = true;
			
		}catch(PDOException $e){
			$pdo->rollBack();
			$pdo = null;
		}
		$pdo = null;
		return $ret;
	
	}







}