<?php

/* user controll */

class userCreate extends DatabaseBase{
	static public function add($nickname,$mailaddress,$login_pass,$salt){
		$ret = 0;
		try{
			$pdo = new PDO(self::getUpdateConnectionString(), self::$upd_db_user, self::$upd_db_password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$sql = 'insert into `users` ('.
			       '   `nickname`, '.
			       '   `mailaddress`, '.
			       '   `login_pass`, '.
			       '   `salt`, '.
			       '   `created_at`, '.
			       ' )values('.
			       '   :nickname, '.
			       '   :mailaddress, '.
			       '   :login_pass, '.
			       '   :salt, '.
			       '   :created_at, '.
			       ' )';
			
			$pdo->beginTransaction();
			
			//文字化け対策
			$stmt = $pdo->query("SET NAMES utf8;");
			
			$stmt = $pdo->prepare($sql);
			
			//パラメータバインド
			$salt = sha1($mailaddress.mt_rand(1, 999999);
			$login_pass = sha1($_POST['password'].$salt);
			
			$stmt->bindValue('nickname', $nickname, PDO::PARAM_STR);
			$stmt->bindValue('mailaddress', $mailaddress, PDO::PARAM_STR);
			$stmt->bindValue('login_pass', $login_pass, PDO::PARAM_STR);
			$stmt->bindValue('salt', $salt, PDO::PARAM_STR);
			$stmt->bindValue('created_at', strtotime (date ("Y-m-d H:i:s")), PDO::PARAM_STR);
			
			if($stmt->execute() == true){
				$ret = $pdo->lastInsertId();
				$pdo->commit();
			}else{
				$ret = null;
				$pdo->rollBack();
			}
			
		}catch(PDOException $e){
			$pdo->rollBack();
			//Logger::fatal($_SERVER['REQUEST_URI'], print_r($_REQUEST, true), $_SERVER['REMOTE_ADDR'], 'データベースエラー', $e);
			$pdo = null;
		}
		$pdo = null;
	
	    return (int)$ret;

    }
}
