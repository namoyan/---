<?php

include_once dirname(__FILE__).'/../common/ClassLoader.php';

class DatabaseBase extends BaseClass{
    static protected $db_name = CommonDefine::DB_NAME;
    static protected $db_address = CommonDefine::DB_ADDRESS;
    static public $db_user = CommonDefine::DB_USER;
    static public $db_password = CommonDefine::DB_PASSWORD;
    
    static protected $upd_db_name = CommonDefine::UPD_DB_NAME;
    static protected $upd_db_address = CommonDefine::UPD_DB_ADDRESS;
    static public $upd_db_user = CommonDefine::UPD_DB_USER;
    static public $upd_db_password = CommonDefine::UPD_DB_PASSWORD;
    
    static public function getConnectionString(){
        return 'mysql:dbname='.self::$db_name.';host='.self::$db_address;
    }
    
    static public function getUpdateConnectionString(){
        return 'mysql:dbname='.self::$upd_db_name.';host='.self::$upd_db_address;
    }
    
    static protected function toHtmlChars($str){
        return htmlspecialchars($str,  ENT_QUOTES, 'UTF-8');
    }
    
}