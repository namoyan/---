
<?php
class jsonRecieve{
    const SUCCESS_STATUS_HEADER = 'HTTP/1.0 200';
    const SUCCESS_STATUS_CODE = '200';
    
    const NOT_ENOUGH_HEADER = 'HTTP/1.0 400';
    const NOT_ENOUGH_CODE = '400';
    const NOT_ENOUGH_MESSAGE = 'NotEnoughParameters';
    
    const ILLEGAL_HEADER = 'HTTP/1.0 400';
    const ILLEGAL_CODE = '400';
    const ILLEGAL_HEADER_MESSAGE = 'IllegalParameters';
    
    const NOT_SSL_HEADER = 'HTTP/1.0 400';
    const NOT_SSL_CODE = '400';
    const NOT_SSL_MESSAGE = 'RequestIsNotSsl';
    
    const NOT_AVAILLABLE_HEADER = 'HTTP/1.0 403';
    const NOT_AVAILLABLE_CODE = '403';
    const NOT_AVAILLABLE_MESSAGE = 'NotAvaillable';
    
    const AUTHORIZATION_HEADER = 'HTTP/1.0 401';
    const AUTHORIZATION_CODE = '401';
    const AUTHORIZATION_MESSAGE = 'Authorization Required';
    
    const USER_NOT_FOUND_HEADER = 'HTTP/1.0 404';
    const USER_NOT_FOUND_CODE = '404';
    const USER_NOT_FOUND_MESSAGE = 'UserNotFound';
    
    const NOT_FOUND_HEADER = 'HTTP/1.0 404';
    const NOT_FOUND_CODE = '404';
    const NOT_FOUND_MESSAGE = 'UserNotFound';
    const COURSE_NOT_FOUND_MESSAGE = 'CourseNotFound';
    
    const INTERNAEL_SERVER_HEADER = 'HTTP/1.0 500';
    const INTERNAEL_SERVER_CODE = '500';
    const INTERNAEL_SERVER_MESSAGE = 'InternalServerError';
    
    const MAINTENANCE_HEADER = 'HTTP/1.0 503';
    const MAINTENANCE_CODE = '503';
    const MAINTENANCE_MESSAGE = 'ServerIsMaintenanceMode';
    
    const DISABLE_HEADER = 'HTTP/1.0 426';
    const DISABLE_CODE = '426';
    const DISABLE_MESSAGE = 'ApplicationVersionIsNotSupported';

	function userReceive($code = null,$message,$retarr){
		
		$header['code'] = $code;
		$header['messages'] = $message;
		$body['user'] = $retarr;

		
		$rec = $this->jsonEncodeArr($header,$body);
		
		return $rec;
	}

	function jsonEncodeArr($header,$body){
		$rec = json_encode(array($header,$body));
		
		return $rec;
	}

} 