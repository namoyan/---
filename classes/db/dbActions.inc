<?php
/*
|   DB操作クラス
|   author:namoyan303@gmail.com
|   create:130822
*/

class dbActions{

	private $dbconn = false;
	
	private function __construct() {
		if(empty($dbconn)) $dbconn = mysql_connect(DB_ADDRESS,DB_USER,DB_PASSWORD);
		mysql_select_db(DB_NAME,$dbconn);
		
		$sql = "SET AUTOCOMMIT=0";
		mysql_query($sql);
	}

	private function query($sql){
		$sql = "BEGIN";
		mysql_query($sql);
		
		$res = mysql_query($sql,$dbconn) or die(mysql_error());
		
		if($res){
			$sql = "COMMIT";
			mysql_query($sql);
		}else{
			$sql = "ROLLBACK";
			mysql_query($sql);
		}
		
		return $res;
	}

	private function insert($sql){
		$sql = "BEGIN";
		mysql_query($sql);
		
		$res = mysql_query($sql,$dbconn) or die(mysql_error());

		if($res){
			$res = mysql_insert_id($dbconn);
			$sql = "COMMIT";
			mysql_query($sql);
		}else{
			$sql = "ROLLBACK";
			mysql_query($sql);
		}
		
		return $res;
	}
	
	private function update($sql){
		$sql = "BEGIN";
		mysql_query($sql);
		
		$res = mysql_query($sql,$dbconn) or die(mysql_error());

		if($res){
			$res = mysql_affected_rows($dbconn);
			$sql = "COMMIT";
			mysql_query($sql);
		}else{
			$sql = "ROLLBACK";
			mysql_query($sql);
		}

		return $res;
	}
	
	private function select($sql){
		$res = mysql_query($sql,$dbconn) or die(mysql_error());
		//$row = array("0"=>"");
		while($ress = mysql_fetch_assoc($res)){
			if($ress != ""){
				if($ress['id']){
					$id = $ress['id'];
					$row[$id] = $ress;
				}else{
					$row = $ress;
				}
			}
		}
		return $row;
	}
	
	private function sel_key($sql){
		$res = mysql_query($sql,$dbconn) or die(mysql_error());
		//$row = array("0"=>"");
		while($ress = mysql_fetch_assoc($res)){
			if($ress != ""){
				$row[] = $ress;
			}
		}
		return $row;
	}
	
	private function sel_one($sql){
		$res = mysql_query($sql,$dbconn) or die(mysql_error());
		while($ress = mysql_fetch_assoc($res)){
			if($ress != ""){
				$row = $ress;
			}
		}
		return $row;
	}
}