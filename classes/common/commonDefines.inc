<?php

/* DEFINE */

class commonDefines{
	
    //データベースユーザー名
    const DB_USER = '';
    
    //データベースパスワード
    const DB_PASSWORD = '';
    
    //データベース名
    const DB_NAME = 'kibun';
    
    //データベースアドレス
    const DB_ADDRESS = 'localhost';
}
