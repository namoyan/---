<?php
include_once dirname(__FILE__).'/CommonDefine.php';

    function __autoload($class_name) {
	
        if(file_exists(__autoload_file_path('/auth/', $class_name))){
            include_once __autoload_file_path('/auth/', $class_name);
        }
        
        if(file_exists(__autoload_file_path('/base/', $class_name))){
            include_once __autoload_file_path('/base/', $class_name);
        }
        if(file_exists(__autoload_file_path('/common/', $class_name))){
            include_once __autoload_file_path('/common/', $class_name);
        }
        
        if(file_exists(__autoload_file_path('/db/', $class_name))){
            
            include_once __autoload_file_path('/db/', $class_name);
        }
        
        if(file_exists(__autoload_file_path('/json/', $class_name))){
            include_once __autoload_file_path('/json/', $class_name);
        }
        
        if(file_exists(__autoload_file_path('/user/', $class_name))){
            include_once __autoload_file_path('/user/', $class_name);
        }
        
        if(file_exists(__autoload_file_path('/', $class_name))){
            include_once __autoload_file_path('/', $class_name);
        }

    }
    
    function __autoload_file_path($dir, $class_name) {
        return CommonDefine::APP_ROOT_DIR.CommonDefine::APP_CLASSES_DIR.$dir.$class_name.'.php';
    }
